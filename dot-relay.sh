#!/usr/bin/env bash

# Gets the working directory where the dot-relay script is.
# Prevents having to type the dns-tls-relay-master directory.
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )" # http://stackoverflow.com/a/246128

# Enforce the “Fail-Fast” Principle
set -o errexit
set -o nounset
set -o pipefail

# Get/Set the directory for the dns over tls relay
change_directory() {
#  printf "Please specify the DNS TLS RELAY directory : \n"
#  read -r DIR
  cd "$DIR" || exit
}

# Gets the interface IP that will be used as the DNS server
get_interface_ip() {
  printf "Enter interface IP here (ex. 127.0.0.1): \n"
  read -r interface_ip
}

# User provides the DoT supported DNS server (Cloudflare, Quad9, etc) as long as it supports DoT
set_dot_server() {
  printf "Enter your preferred primary DNS provider here (ex. 9.9.9.9):\n"
  read -r primary_dns

  printf "Enter your preferred secondary DNS provider here (ex. 149.112.112.112):\n"
  read -r secondary_dns
}

# user provides keep alive amount they want (4, 6, 8)
set_keep_alive() {
  printf "Enter your preferred keep alive here (4, 6, 8):\n"
  read -r keep_alive
}

# running the python script with the requested arguments
run_with_arguments() {
  printf '\033]2;DNS TLS Relay\a' # literally just changes the title of terminal
  python3 ./run_relay.py -l"$interface_ip" -r "$primary_dns" "$secondary_dns" -k"$keep_alive" -c -v
}

die() {
  local msg=$1
  local code=${2-1} # default exit status 1
  printf '%s' "$msg"
  exit "$code"
}

main() {
  # check if script ran as sudo
  if [ "$UID" -gt 0 ]; then
    printf "Sorry, this script must be run as ROOT!\n"
    die "1"
  fi
    clear
    
    printf '\033]2;dns tls relay assistant\a\n'
    printf "
|-----------------------------------------------------|
|                                                     |
|          :: dns tls relay assistant ::              |
|                                  by zach            |
|                                                     |
|-----------------------------------------------------|
"
  change_directory
  get_interface_ip
  set_dot_server
  set_keep_alive

  run_with_arguments
  printf "... dns-tls-assistant complete ...\n"
}

main