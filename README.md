# dns tls relay assistant
  - This script is a user friendly way of running DOWRIGHT's DNS TLS Relay (privacy proxy).

# Requirements
  1. Windows 10 & Linux VM (in bridged networking mode)
  2. [DNS TLS RELAY](https://github.com/DOWRIGHTTV/dns-tls-relay)

# How To Install/Use?
## Windows 10 (w/ Linux VM)
  1. Create a Linux VM (Make that VM use bridged networking mode).
  2. Download DNS TLS Relay and this script.
  3. Set your preferred Primary DNS to "127.0.0.1" in Network & Internet > Change adapter options > Right click your adapter > Properties > Internet Protocol Version 4 (TCP/IPv4) > Properties.
  4. Move this script into the dns-tls-relay-master directory and open a terminal in this directory.
  5. Enter "chmod +x dot-relay.sh" hit enter.
  6. Enter "sudo ./dot-relay.sh" hit enter, run through setup.
  7. Enjoy!

## Linux Terminal
  1. Download DNS TLS Relay and this script.
  2. Set your preferred Primary DNS to "127.0.0.1" (you may need to run 'sudo systemctl restart NetworkManager.service' after setting the DNS)
  3. Move this script into the dns-tls-relay-master directory and open a terminal in this directory.
  4. Enter "chmod +x dot-relay.sh" hit enter.
  5. Enter "sudo ./dot-relay.sh" hit enter, run through setup.
  6. Enjoy!

 
# Why Did I Make This
  - I decided to create this script because I wanted to have a simple way of running the DNS TLS Relay instead of having to remember the command, as I cannot just let my computer run 24/7.

# How It Works
  - This script is basically a wrapper for running the [DNS TLS RELAY](https://github.com/DOWRIGHTTV/dns-tls-relay). 

# TODO
  - Somehow make the script able to save configurations and automatically run with that config (for ease of use).
  - Improve this script as much as possible.

# Known Issues
  - If you run this and then shutdown your PC, Your PC will be disconnected until you open your Linux VM running this script. Best solution to this is either purchase a raspberry pi and run it on there or set the VM to run at startup.

# Credits
  - [DOWRIGHT](https://www.github.com/DOWRIGHTTV) - DNS TLS RELAY

# Contribute
  - You can contribute to this project by helping me improve this script in anyway possible. Thanks!